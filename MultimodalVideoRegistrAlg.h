#pragma once

#include <opencv2/opencv.hpp>
#include <opencv2/shape.hpp>

/*!
 *
 *  MultimodalVideoRegistrAlg v1.0: planar registration only, based on homography estimation
 *
 *  For more details, refer to the 2015 CVPRW (PBVS) paper 'Online Multimodal Video Registration
 *  Based on Shape Matching'. Note that this algorithm relies on OpenCV >= 3.0.0a, patched with
 *  the diff file provided with the rest of the source code.
 *
 */
class MultimodalVideoRegistrAlg {
public:
    //! default constructor, also readies all internal structures for processing
    MultimodalVideoRegistrAlg();
    //! resets all internal structures (i.e. correspondence buffers, reference transformations, smoothing weights)
    void ResetInternalState();
    //! extracts foreground shapes from fg-bg segmentation masks and attempts to register them
    void ProcessForeground(const cv::Mat& oForeground_ToTransform, const cv::Mat& oForeground);

    //! returns the latest extracted foreground object contours
    inline const std::vector<std::vector<cv::Point> >& GetLatestContours(bool bToTransform) const {return bToTransform?m_vvoBlobContours_ToTransform:m_vvoBlobContours;}
    //! returns the latest estimated homography
    inline const cv::Mat& GetTransformationMatrix(bool bInvert) const {return bInvert?m_oBestTransMat_inv:m_oBestTransMat;}

    //! utility: paints foreground regions based on contour points lists
    static void PaintFGRegions(const std::vector<std::vector<cv::Point> >& voPoints, cv::Scalar oPointColor, cv::Scalar oEdgeColor, cv::Mat& oOutput);

private:
    cv::Ptr<cv::ShapeContextDistanceExtractor> m_pShapeDistExtr;
    cv::Ptr<cv::ShapeTransformer> m_pShapeTransf_TPS,m_pShapeTransf_Aff;
    std::vector<std::vector<cv::Point> > m_vvoRecentFiltKeyPoints_ToTransform,m_vvoRecentFiltKeyPoints;
    std::vector<std::vector<cv::Point> > m_vvoBlobContours_ToTransform, m_vvoBlobContours;
    int m_nSmoothingCount;
    bool m_bDebug;
    bool m_bLoopedAtLeastOnce;
    size_t m_nCurrFrameStackIdx;
    size_t m_nCurrModelNextCoordIdx;
    size_t m_nCurrModelLastCoordIdx;
    float m_fPrevBestForegroundOverlapError;
    std::vector<float> m_vfModelCoords_ToTransform,m_vfModelCoords;
    std::vector<int> m_vnModelCoordsPersistence;
    cv::Mat m_oKeyPoints_ToTransform,m_oKeyPoints;
    cv::Mat m_oLatestTransMat,m_oLatestTransMat_inv;
    cv::Mat m_oBestTransMat,m_oBestTransMat_inv;
    cv::Mat m_oCurrTransMat,m_oCurrTransMat_inv;
    cv::Mat m_oLatestTransformedForeground;
    cv::Mat m_oBestTransformedForeground;

    template<typename T> inline static void ReallocModelVector(std::vector<T>& vtModel, size_t nNewMaxModelSize, size_t nCurrModelNextIdx, size_t nCurrModelLastIdx) {
        const size_t nOldMaxModelCoordsCount = vtModel.size();
        std::vector<T> vtNewModel(nNewMaxModelSize);
        const size_t nFirstBlockLength = nOldMaxModelCoordsCount-nCurrModelLastIdx;
        memcpy(vtNewModel.data(),vtModel.data()+nCurrModelLastIdx,sizeof(float)*nFirstBlockLength);
        if(nCurrModelNextIdx>0) {
            const size_t nSecondBlockLength = nCurrModelNextIdx;
            memcpy(vtNewModel.data()+nFirstBlockLength,vtModel.data(),sizeof(float)*nSecondBlockLength);
        }
        vtModel = vtNewModel;
    }
};
