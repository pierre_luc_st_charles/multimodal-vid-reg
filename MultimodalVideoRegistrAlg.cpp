#include "MultimodalVideoRegistrAlg.h"
#include "DatasetUtils.h"

// minimum foreground blob area required for contour point matching
#define MIN_BLOB_AREA                   100
// approximative frame size of the correspondance buffer
#define NB_FRAME_MEMORY                 100
// approximative contour point count in a single frame (used for init only)
#define KEYPTS_PER_FRAME_BUFFER         100
// minimum number of contour points required for basic transformation estimation
#define MIN_KEYPOINTS_COUNT             5
// minimum number of contour points required for TPS model estimation
#define MIN_TPS_KEYPOINTS_COUNT         25
// minimum blob shape match similarity score required to add contour points to buffer
#define MIN_SIMILARITY_SCORE            1.0f
// initial/default value for smoothing count variable ('alpha' in the paper)
#define DEFAULT_SMOOTH_COUNT            2
// defines whether to match shapes frame-wide or blob-per-blob
#define USE_PER_BLOB_SHAPE_DIST         1
// defines whether to invert the FG region size ratio test or not
#define INVERT_FG_PROPORTION            0

MultimodalVideoRegistrAlg::MultimodalVideoRegistrAlg() {
    ResetInternalState();
}

void MultimodalVideoRegistrAlg::ResetInternalState() {
    m_pShapeDistExtr = cv::createShapeContextDistanceExtractor(15,5,0.1f,4.0f);
    m_pShapeTransf_TPS = cv::createThinPlateSplineShapeTransformer();
    m_pShapeTransf_Aff = cv::createAffineTransformer(false);
    m_vvoRecentFiltKeyPoints_ToTransform = std::vector<std::vector<cv::Point> >(NB_FRAME_MEMORY);
    m_vvoRecentFiltKeyPoints = std::vector<std::vector<cv::Point> >(NB_FRAME_MEMORY);
    m_nSmoothingCount = DEFAULT_SMOOTH_COUNT;
    m_bLoopedAtLeastOnce = false;
    m_nCurrFrameStackIdx = 0;
    m_nCurrModelNextCoordIdx = 2;
    m_nCurrModelLastCoordIdx = 0;
    m_fPrevBestForegroundOverlapError = 1.0f;
    m_vfModelCoords_ToTransform = std::vector<float>(NB_FRAME_MEMORY*KEYPTS_PER_FRAME_BUFFER,0.0f);
    m_vfModelCoords = std::vector<float>(NB_FRAME_MEMORY*KEYPTS_PER_FRAME_BUFFER,0.0f);
    m_vnModelCoordsPersistence = std::vector<int>(m_vfModelCoords_ToTransform.size()/2,0);
    m_oLatestTransMat = cv::Mat();
    m_oLatestTransMat_inv = cv::Mat();
    m_oBestTransMat = cv::Mat::eye(3,3,CV_64FC1);
    m_oBestTransMat_inv = cv::Mat::eye(3,3,CV_64FC1);
    m_oCurrTransMat = cv::Mat();
    m_oCurrTransMat_inv = cv::Mat();
}

void MultimodalVideoRegistrAlg::ProcessForeground(const cv::Mat& oForeground_ToTransform, const cv::Mat& oForeground) {
    cv::findContours(oForeground_ToTransform,m_vvoBlobContours_ToTransform,cv::RETR_LIST,cv::CHAIN_APPROX_SIMPLE);
    cv::findContours(oForeground,m_vvoBlobContours,cv::RETR_LIST,cv::CHAIN_APPROX_SIMPLE);
    if(!m_vvoBlobContours_ToTransform.empty() && !m_vvoBlobContours.empty()) {
        std::vector<std::vector<cv::Point> > vvoFiltBlobContours_ToTransform, vvoFiltBlobContours;
        std::vector<std::vector<cv::DMatch> > vvoFiltBlobKeyPointsMatches;
#if USE_PER_BLOB_SHAPE_DIST
        for(size_t n=0; n<m_vvoBlobContours_ToTransform.size(); ++n) {
            float fMinBlobSimilarityScore = FLT_MAX;
            std::vector<cv::DMatch> voBestBlobMatches;
            size_t nBestContourIdx = 0;
            for(size_t m=0; m<m_vvoBlobContours.size(); ++m) {
                if(m_vvoBlobContours_ToTransform[n].size()>MIN_TPS_KEYPOINTS_COUNT && m_vvoBlobContours[m].size()>MIN_TPS_KEYPOINTS_COUNT)
                    m_pShapeDistExtr->setTransformAlgorithm(m_pShapeTransf_TPS);
                else if(m_vvoBlobContours_ToTransform[n].size()>MIN_KEYPOINTS_COUNT && m_vvoBlobContours[m].size()>MIN_KEYPOINTS_COUNT)
                    m_pShapeDistExtr->setTransformAlgorithm(m_pShapeTransf_Aff);
                else
                    continue;
                float fCurrBlobSimilarityScore = m_pShapeDistExtr->computeDistance(m_vvoBlobContours_ToTransform[n],m_vvoBlobContours[m]);
                if(fCurrBlobSimilarityScore<fMinBlobSimilarityScore) {
                    fMinBlobSimilarityScore = fCurrBlobSimilarityScore;
                    std::vector<cv::DMatch> voCurrBlobMatches = m_pShapeDistExtr->getLatestMatches();
                    voBestBlobMatches.clear();
                    for(size_t k=0; k<voCurrBlobMatches.size(); ++k)
                        if(voCurrBlobMatches[k].queryIdx<(int)m_vvoBlobContours_ToTransform[n].size() && voCurrBlobMatches[k].trainIdx<(int)m_vvoBlobContours[m].size())
                            voBestBlobMatches.push_back(voCurrBlobMatches[k]);
                    nBestContourIdx = m;
                }
            }
            if(fMinBlobSimilarityScore<MIN_SIMILARITY_SCORE && !voBestBlobMatches.empty()) {
                vvoFiltBlobContours_ToTransform.push_back(m_vvoBlobContours_ToTransform[n]);
                vvoFiltBlobContours.push_back(m_vvoBlobContours[nBestContourIdx]);
                vvoFiltBlobKeyPointsMatches.push_back(voBestBlobMatches);
            }
        }
#else //!USE_PER_BLOB_SHAPE_DIST
        std::vector<cv::Point> voFullBlobContours_ToTransform, voFullBlobContours;
        for(size_t n=0; n<m_vvoBlobContours_ToTransform.size(); ++n)
            voFullBlobContours_ToTransform.insert(voFullBlobContours_ToTransform.end(),m_vvoBlobContours_ToTransform[n].begin(),m_vvoBlobContours_ToTransform[n].end());
        for(size_t m=0; m<m_vvoBlobContours.size(); ++m)
            voFullBlobContours.insert(voFullBlobContours.end(),m_vvoBlobContours[m].begin(),m_vvoBlobContours[m].end());
        if(voFullBlobContours_ToTransform.size()>MIN_TPS_KEYPOINTS_COUNT && voFullBlobContours.size()>MIN_TPS_KEYPOINTS_COUNT) {
            float fCurrBlobSimilarityScore = pShapeDistExtr->computeDistance(voFullBlobContours_ToTransform,voFullBlobContours);
            std::vector<cv::DMatch> voBlobMatches, voCurrBlobMatches = pShapeDistExtr->getLatestMatches();
            for(size_t k=0; k<voCurrBlobMatches.size(); ++k)
                if(voCurrBlobMatches[k].queryIdx<(int)voFullBlobContours_ToTransform.size() && voCurrBlobMatches[k].trainIdx<(int)voFullBlobContours.size())
                    voBlobMatches.push_back(voCurrBlobMatches[k]);
            if(fCurrBlobSimilarityScore<MIN_SIMILARITY_SCORE && !voBlobMatches.empty()) {
                vvoFiltBlobContours_ToTransform.push_back(voFullBlobContours_ToTransform);
                vvoFiltBlobContours.push_back(voFullBlobContours);
                vvoFiltBlobKeyPointsMatches.push_back(voBlobMatches);
            }
        }
#endif //!USE_PER_BLOB_SHAPE_DIST
        if(!vvoFiltBlobContours_ToTransform.empty() && !vvoFiltBlobContours.empty()) {
            CV_Assert(vvoFiltBlobContours_ToTransform.size()==vvoFiltBlobContours.size());
            size_t nCurrKeyPointsCount=0;
            for(size_t n=0; n<vvoFiltBlobContours_ToTransform.size(); ++n)
                nCurrKeyPointsCount += vvoFiltBlobKeyPointsMatches[n].size();
            if(nCurrKeyPointsCount>0) {
                m_vvoRecentFiltKeyPoints_ToTransform[m_nCurrFrameStackIdx].resize(nCurrKeyPointsCount);
                m_vvoRecentFiltKeyPoints[m_nCurrFrameStackIdx].resize(nCurrKeyPointsCount);
                for(size_t n=0,nKeyPointIdx=0; n<vvoFiltBlobContours_ToTransform.size(); ++n) {
                    for(size_t m=0; m<vvoFiltBlobKeyPointsMatches[n].size(); ++m) {
                        m_vvoRecentFiltKeyPoints_ToTransform[m_nCurrFrameStackIdx][nKeyPointIdx] = vvoFiltBlobContours_ToTransform[n][vvoFiltBlobKeyPointsMatches[n][m].queryIdx];
                        m_vvoRecentFiltKeyPoints[m_nCurrFrameStackIdx][nKeyPointIdx] = vvoFiltBlobContours[n][vvoFiltBlobKeyPointsMatches[n][m].trainIdx];
                        ++nKeyPointIdx;
                    }
                }
                const size_t nCurrModelCoordsCount = (m_nCurrModelLastCoordIdx<m_nCurrModelNextCoordIdx)?(m_nCurrModelNextCoordIdx-m_nCurrModelLastCoordIdx):(m_vfModelCoords_ToTransform.size()-m_nCurrModelLastCoordIdx+m_nCurrModelNextCoordIdx);
                if(nCurrModelCoordsCount+nCurrKeyPointsCount*2>m_vfModelCoords_ToTransform.size()) {
                    const size_t nOldMaxModelCoordsCount = m_vfModelCoords_ToTransform.size();
                    const size_t nNewMaxModelCoordsCount = (nOldMaxModelCoordsCount+nCurrKeyPointsCount*2)*2;
                    ReallocModelVector(m_vfModelCoords_ToTransform,nNewMaxModelCoordsCount,m_nCurrModelNextCoordIdx,m_nCurrModelLastCoordIdx);
                    ReallocModelVector(m_vfModelCoords,nNewMaxModelCoordsCount,m_nCurrModelNextCoordIdx,m_nCurrModelLastCoordIdx);
                    ReallocModelVector(m_vnModelCoordsPersistence,nNewMaxModelCoordsCount/2,m_nCurrModelNextCoordIdx/2,m_nCurrModelLastCoordIdx/2);
                    m_nCurrModelNextCoordIdx = nCurrModelCoordsCount;
                    m_nCurrModelLastCoordIdx = 0;
                }
                float* pfCoords_ToTransform = m_vfModelCoords_ToTransform.data();
                float* pfCoords = m_vfModelCoords.data();
                int* pnPersistence = m_vnModelCoordsPersistence.data();
                const size_t nMaxModelCoordsCount = m_vfModelCoords_ToTransform.size();
                if(!m_bLoopedAtLeastOnce) {
                    for(size_t n=0; n<nCurrKeyPointsCount; ++n) {
                        pnPersistence[m_nCurrModelNextCoordIdx/2] = 0;
                        pfCoords_ToTransform[m_nCurrModelNextCoordIdx] = (float)m_vvoRecentFiltKeyPoints_ToTransform[m_nCurrFrameStackIdx][n].x;
                        pfCoords[m_nCurrModelNextCoordIdx] = (float)m_vvoRecentFiltKeyPoints[m_nCurrFrameStackIdx][n].x;
                        ++m_nCurrModelNextCoordIdx;
                        pfCoords_ToTransform[m_nCurrModelNextCoordIdx] = (float)m_vvoRecentFiltKeyPoints_ToTransform[m_nCurrFrameStackIdx][n].y;
                        pfCoords[m_nCurrModelNextCoordIdx] = (float)m_vvoRecentFiltKeyPoints[m_nCurrFrameStackIdx][n].y;
                        ++m_nCurrModelNextCoordIdx %= nMaxModelCoordsCount;
                    }
                }
                else {
                    if(m_nCurrModelLastCoordIdx>=m_nCurrModelNextCoordIdx) {
                        ReallocModelVector(m_vfModelCoords_ToTransform,m_vfModelCoords_ToTransform.size(),m_nCurrModelNextCoordIdx,m_nCurrModelLastCoordIdx);
                        pfCoords_ToTransform = m_vfModelCoords_ToTransform.data();
                        ReallocModelVector(m_vfModelCoords,m_vfModelCoords.size(),m_nCurrModelNextCoordIdx,m_nCurrModelLastCoordIdx);
                        pfCoords = m_vfModelCoords.data();
                        ReallocModelVector(m_vnModelCoordsPersistence,m_vnModelCoordsPersistence.size(),m_nCurrModelNextCoordIdx/2,m_nCurrModelLastCoordIdx/2);
                        pnPersistence = m_vnModelCoordsPersistence.data();
                        m_nCurrModelNextCoordIdx = nCurrModelCoordsCount;
                        m_nCurrModelLastCoordIdx = 0;
                    }
                    cv::Mat oPersistenceMap(1,m_nCurrModelNextCoordIdx/2,CV_32SC1,pnPersistence);
                    double dMinPersistenceVal,dMaxPersistenceVal;
                    cv::minMaxIdx(oPersistenceMap,&dMinPersistenceVal,&dMaxPersistenceVal);
                    if(dMinPersistenceVal>0)
                        oPersistenceMap -= (int)dMinPersistenceVal;
                    for(size_t n=0; n<nCurrKeyPointsCount; ++n) {
                        const size_t nRandModelCoordIdx = m_nCurrModelLastCoordIdx+((rand()*2)%m_nCurrModelNextCoordIdx);
                        if(pnPersistence[nRandModelCoordIdx/2]<=0) {
                            pfCoords_ToTransform[nRandModelCoordIdx] = (float)m_vvoRecentFiltKeyPoints_ToTransform[m_nCurrFrameStackIdx][n].x;
                            pfCoords[nRandModelCoordIdx] = (float)m_vvoRecentFiltKeyPoints[m_nCurrFrameStackIdx][n].x;
                            pfCoords_ToTransform[nRandModelCoordIdx+1] = (float)m_vvoRecentFiltKeyPoints_ToTransform[m_nCurrFrameStackIdx][n].y;
                            pfCoords[nRandModelCoordIdx+1] = (float)m_vvoRecentFiltKeyPoints[m_nCurrFrameStackIdx][n].y;
                        }
                    }
                }
                CV_Assert((m_nCurrModelNextCoordIdx%2)==0 && (m_nCurrModelLastCoordIdx%2)==0);
                const size_t nNewModelCoordsCount = (m_nCurrModelLastCoordIdx<m_nCurrModelNextCoordIdx)?(m_nCurrModelNextCoordIdx-m_nCurrModelLastCoordIdx):(m_vfModelCoords_ToTransform.size()-m_nCurrModelLastCoordIdx+m_nCurrModelNextCoordIdx);
                if(nNewModelCoordsCount>8) { // need at least 8 correspondances to determine a general homography transformation (ignoring scaling factor)
                    if(m_nCurrModelLastCoordIdx<m_nCurrModelNextCoordIdx) {
                        m_oKeyPoints_ToTransform = cv::Mat((m_nCurrModelNextCoordIdx-m_nCurrModelLastCoordIdx)/2,1,CV_32FC2,pfCoords_ToTransform+m_nCurrModelLastCoordIdx);
                        m_oKeyPoints = cv::Mat((m_nCurrModelNextCoordIdx-m_nCurrModelLastCoordIdx)/2,1,CV_32FC2,pfCoords+m_nCurrModelLastCoordIdx);
                    }
                    else {
                        CV_Assert(!m_bLoopedAtLeastOnce);
                        m_oKeyPoints_ToTransform.create((nMaxModelCoordsCount-m_nCurrModelLastCoordIdx+m_nCurrModelNextCoordIdx)/2,1,CV_32FC2);
                        m_oKeyPoints.create((nMaxModelCoordsCount-m_nCurrModelLastCoordIdx+m_nCurrModelNextCoordIdx)/2,1,CV_32FC2);
                        const size_t nFirstBlockLength = nMaxModelCoordsCount-m_nCurrModelLastCoordIdx;
                        memcpy(m_oKeyPoints_ToTransform.data,pfCoords_ToTransform+m_nCurrModelLastCoordIdx,sizeof(float)*nFirstBlockLength);
                        memcpy(m_oKeyPoints.data,pfCoords+m_nCurrModelLastCoordIdx,sizeof(float)*nFirstBlockLength);
                        if(m_nCurrModelNextCoordIdx>0) {
                            const size_t nSecondBlockLength = m_nCurrModelNextCoordIdx;
                            memcpy(((float*)m_oKeyPoints_ToTransform.data)+nFirstBlockLength,pfCoords_ToTransform,sizeof(float)*nSecondBlockLength);
                            memcpy(((float*)m_oKeyPoints.data)+nFirstBlockLength,pfCoords,sizeof(float)*nSecondBlockLength);
                        }
                    }
                    cv::Mat oOutliersMask;
                    m_oCurrTransMat_inv = cv::findHomography(m_oKeyPoints_ToTransform,m_oKeyPoints,oOutliersMask,cv::RANSAC);
                    if(!m_oCurrTransMat_inv.empty()) {
                        cv::invert(m_oCurrTransMat_inv,m_oCurrTransMat);
                        m_oLatestTransMat = m_oCurrTransMat.clone();
                        m_oLatestTransMat_inv = m_oCurrTransMat_inv.clone();
                        for(size_t n=m_nCurrModelLastCoordIdx, nLocalIdx=0; n!=m_nCurrModelNextCoordIdx; (n+=2)%=nMaxModelCoordsCount)
                            if(oOutliersMask.at<uchar>(nLocalIdx++))
                                ++pnPersistence[n/2];
                            else
                                --pnPersistence[n/2];
                    }
                }
                ++m_nCurrFrameStackIdx %= NB_FRAME_MEMORY;
                if(!m_bLoopedAtLeastOnce && m_nCurrFrameStackIdx==0)
                    m_bLoopedAtLeastOnce = true;
                if(!m_bLoopedAtLeastOnce)
                    (m_nCurrModelLastCoordIdx+=m_vvoRecentFiltKeyPoints_ToTransform[m_nCurrFrameStackIdx].size()*2) %= nMaxModelCoordsCount;
            }
        }
    }
    if(!m_oLatestTransMat.empty()) {
        const cv::Size oTransformedImageSize = oForeground.size();
        m_oLatestTransformedForeground.create(oTransformedImageSize,CV_8UC1);
        m_oBestTransformedForeground.create(oTransformedImageSize,CV_8UC1);
        cv::warpPerspective(oForeground_ToTransform,m_oLatestTransformedForeground,m_oLatestTransMat,oTransformedImageSize,cv::INTER_NEAREST|cv::WARP_INVERSE_MAP,cv::BORDER_CONSTANT);
        cv::warpPerspective(oForeground_ToTransform,m_oBestTransformedForeground,m_oBestTransMat,oTransformedImageSize,cv::INTER_NEAREST|cv::WARP_INVERSE_MAP,cv::BORDER_CONSTANT);
        const int nForegroundPxCount = cv::countNonZero(oForeground);
        const int nForegroundPxCount_ToTransform = cv::countNonZero(oForeground_ToTransform);
        const float fLatestForegroundOverlapError = DatasetUtils::CalcForegroundOverlapError(oForeground,m_oLatestTransformedForeground);
        const float fBestForegroundOverlapError = DatasetUtils::CalcForegroundOverlapError(oForeground,m_oBestTransformedForeground);
        if(fLatestForegroundOverlapError<fBestForegroundOverlapError &&
                (float)cv::countNonZero(m_oLatestTransformedForeground)/((!INVERT_FG_PROPORTION)?nForegroundPxCount:nForegroundPxCount_ToTransform)>(float)nForegroundPxCount/nForegroundPxCount_ToTransform &&
                nForegroundPxCount>MIN_BLOB_AREA && nForegroundPxCount_ToTransform>MIN_BLOB_AREA) {
            if(m_fPrevBestForegroundOverlapError>fLatestForegroundOverlapError || fBestForegroundOverlapError>2*fLatestForegroundOverlapError)
                m_nSmoothingCount = DEFAULT_SMOOTH_COUNT;
            else
                ++m_nSmoothingCount;
            const float fSmoothingFactor = ((float)m_nSmoothingCount-1)/m_nSmoothingCount;
            m_fPrevBestForegroundOverlapError = m_fPrevBestForegroundOverlapError*fSmoothingFactor+fLatestForegroundOverlapError*(1-fSmoothingFactor);
            m_oBestTransMat = m_oBestTransMat*fSmoothingFactor+m_oLatestTransMat*(1-fSmoothingFactor);
            m_oBestTransMat_inv = m_oBestTransMat_inv*fSmoothingFactor+m_oLatestTransMat_inv*(1-fSmoothingFactor);
        }
    }
}

void MultimodalVideoRegistrAlg::PaintFGRegions(const std::vector<std::vector<cv::Point> >& voPoints, cv::Scalar oPointColor, cv::Scalar oEdgeColor, cv::Mat& oOutput) {
    CV_Assert(!oOutput.empty());
    oOutput = cv::Scalar_<uchar>(0,0,0);
    for(size_t k=0; k<voPoints.size(); ++k) {
        for(size_t l=0; l<voPoints[k].size(); ++l) {
            if(!l)
                cv::circle(oOutput,voPoints[k][l],2,cv::Scalar_<uchar>(255,0,0));
            else
                cv::circle(oOutput,voPoints[k][l],2,oPointColor);
            if(l<voPoints[k].size()-1)
                cv::line(oOutput,voPoints[k][l],voPoints[k][l+1],oEdgeColor);
            else
                cv::line(oOutput,voPoints[k][l],voPoints[k][0],oEdgeColor);
        }
    }
}
