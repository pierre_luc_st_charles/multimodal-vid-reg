This directory contains a 'cleaned' version of the multimodal video registration method presented in the
2015 CVPRW paper 'Online Multimodal Video Registration Based on Shape Matching'.

The main algorithm class used to estimate the registration transformation is MultimodalVideoRegistrAlg; 
all other files contain either dependencies, utilities or interfaces. The algorithm is based on OpenCV's
shape module, and has been tested with version 3.0.0-rc1. Some extra interface functions for shape context
and shape transform are needed for this method to work; 'shapecontext_interface_patch_opencv3.0.0-rc1' must
be applied to the opencv code tree in order to add them.

The updated LITIV planar multimodal registration dataset is currently available on Google Drive [1] and 
will soon be available on the LITIV website [2]; extract it locally, and make sure 'LITIV2012_DATASET_PATH'
is well defined in your code before including its associated header utility file 'LITIV2012Utils.h' (see
the provided main.cpp for an example).

See LICENSE.txt for source code terms of use and contact information.

[1] https://drive.google.com/file/d/0B55Ba7lWTLh4ZUx1aWptR1R5eHc/view
[2] https://www.polymtl.ca/litiv/vid/index.php
